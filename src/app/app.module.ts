import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { AppComponent } from "./app.component";
import { ContactsComponent } from "./components/contacts/contacts.component";
import { EditComponent } from "./components/contacts/edit/edit.component";
import { ContactNewComponent } from "./components/contacts/contact-new/contact-new.component";
import { ContactComponent } from "./components/contacts/contact/contact.component";
import { HttpModule } from "@angular/http";
import { HomeComponent } from './components/home/home.component';
import { DeleteComponent } from './components/delete/delete.component';
import { ContactsService } from "./services/contacts.service";
import { ContactsModel } from "./models/contacts/contacts.model";
import { MenuComponent } from './components/menu/menu.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MomentModule } from 'ngx-moment';
import { ContactinfoComponent } from './components/contactinfo/contactinfo.component';
import { FiltersPipe } from './filters/filters.pipe';
import { MyDatePickerModule } from "../../node_modules/mydatepicker/dist/my-date-picker.module";


const routes: Routes = [
  {path:'', component:HomeComponent},
  { path: "contacts", component: ContactsComponent },
  { path: "contacts/new", component: ContactNewComponent },
  {
    path: "contact/:id",component: ContactComponent,
    children: [
    { path: "info", component:ContactinfoComponent },
    { path: "edit", component: EditComponent },
    { path:"delete", component: DeleteComponent}
  ]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    ContactsComponent,
    EditComponent,
    ContactNewComponent,
    ContactComponent,
    HomeComponent,
    DeleteComponent,
    MenuComponent,
    ContactinfoComponent,
    FiltersPipe
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    MomentModule,
    MyDatePickerModule
  ],
  providers: [ContactsService, ContactsModel],
  bootstrap: [AppComponent]
})
export class AppModule {}
