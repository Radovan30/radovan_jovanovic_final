import {  Injectable } from '@angular/core';
import { ContactsService } from '../../services/contacts.service';

@Injectable()

export class ContactsModel { 
  constructor(private contactService:ContactsService){
    this.fillContacts();
  }

  contacts=[];
  contact = {};
  fillContacts(){
    this.contactService.getContactsObservable().subscribe(contacts =>
    {this.contacts = contacts,
    console.log(this.contacts);});
   
    
  }
  getContactById(id,clbk){
      console.log("getContactByid",this.contacts);
      
    if(this.contacts && this.contacts.length > 0){
        for(var i = 0; i < this.contacts.length;i++){
            if(this.contacts[i].id == id){
                clbk(this.contacts[i])
            }
        }
    }else{
        this.refreshContactsClbk(()=>{
            for(var i = 0; i < this.contacts.length;i++){
                if(this.contacts[i].id == id){
                    clbk(this.contacts[i])
                }
            }
        })
    }
    
}

refreshContactsClbk(clbk){
  this.contactService.getContactsObservable().subscribe((contacts) => {
      this.contacts = contacts;
      clbk();
  });
}
  getContact(id){
    this.contactService.getEditContact(id).subscribe(contact =>{
       this.contact = contact;
    })
    
  }
  editContact(data,callbackFukcija){
    console.log("model", data);
    
    this.contactService.updateContactObservable(data).subscribe(() => {
      this.fillContacts();
      callbackFukcija();
    });
  }
  deleteContact(id, callbackFukcija) {
    this.contactService.deleteContactObservable(id).subscribe(() => {
      this.fillContacts();
      callbackFukcija();
    });
  }
  saveContact(contact,clbk){
    this.contactService.addContactObeservable(contact).subscribe((contact)=>{
         this.contacts.push(contact);
         clbk();
    })
 }


}
