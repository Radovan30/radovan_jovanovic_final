import { Component,Input,Output, OnInit,EventEmitter } from "@angular/core";
import { ContactsModel } from "../../models/contacts/contacts.model";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-contacts",
  templateUrl: "./contacts.component.html"
})
export class ContactsComponent implements OnInit {
  searchString: string;
  contact = {};
  constructor(private contactsModel: ContactsModel, private router: Router) {}

  ngOnInit() {}

  editContact(id) {
    console.log(id);

    this.router.navigate(["/contact", id, "edit"]);
  }

  viewDetails(id) {
    console.log(id);

    this.router.navigate(["/contact", id, "info"]);
  }

  deleteContact(id) {
    var r = confirm("Delete contact with ID: "+ id + "?");
    if (r == true) {
      this.contactsModel.deleteContact(id, () => {
        this.router.navigate["/contacts"];
      });
    } else {
      this.router.navigate["/contacts"];
    }
  }
}
