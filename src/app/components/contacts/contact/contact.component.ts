import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-contact",
  templateUrl: "./contact.html"
})
export class ContactComponent implements OnInit {
  id;
  constructor(private route: ActivatedRoute) {
    this.route.params.subscribe(paramsObject => {
      this.id = paramsObject["id"];
      console.log(this.id);
      
    });
  }

  ngOnInit() {}
}
