import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from "@angular/forms";
import  formConfig from "../../../helpers/formsHelper";
import { Router, ActivatedRoute } from "@angular/router";
import { ContactsModel } from "../../../models/contacts/contacts.model";
import { MomentModule } from 'ngx-moment';
import * as moment from 'moment';
import { ContactsService } from '../../../services/contacts.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.html'
})
export class EditComponent implements OnInit {
  editForm:FormGroup;
  form;
  id;
  constructor(private router:Router, 
    private contactsModel:ContactsModel, 
    private route:ActivatedRoute, 
    private contactService:ContactsService) { 
   
  
  }
ngOnInit(){
  this.route.parent.params.subscribe(params => (this.id = params.id));
    this.contactsModel.getContact(this.id);
}
 /*  ngOnInit() {
    this.route.parent.params.subscribe(params => {
      //this.id=params.id
      this.contactsModel.getContactById(params.id, a => {
        console.log("params id",params.id);
        a.birthdate = new Date(a.birthdate).toLocaleDateString();
        console.log(a.birthdate);
        
        this.contact = a;
      });
    });
  } */

  saveContact(id){
    this.contactsModel.editContact(this.contactsModel.contact,()=>{
      this.router.navigate(['/contacts']);
    });
  }

}
