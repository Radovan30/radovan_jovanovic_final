import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import  formConfig from "../../../helpers/formsHelper";
import { Router } from "@angular/router";
import { ContactsModel } from "../../../models/contacts/contacts.model";
import { IMyDpOptions } from '../../../../../node_modules/mydatepicker/dist/interfaces/my-options.interface';
import { NgSwitch } from '@angular/common';

@Component({
  selector: "app-contact-new",
  templateUrl: "./contact-new.component.html"
})
export class ContactNewComponent implements OnInit{
  newForm: FormGroup;
  form;
  submitTried = false;
  public model: any = { date: { year: 2018, month: 10, day: 9 } };
  contact = [];
  constructor( private router:Router, private contactsModel:ContactsModel) {
   
  
  }
  /* for(var i=0;i< this.formConfig.length;i++){
    if(this.formConfig[i].type !== 'subgroup'){
      arrayValidatora = [];
      for(var j=0;j<this.formConfig[i].validators.length;j++){
        arrayValidatora.push(Validators[this.formConfig[i].validators[j]]);
      }
      testFormObject[this.formConfig[i].key] = new FormControl('',arrayValidatora);
    }else{
      var subgroupObject = {}
      for(var k=0;k< this.formConfig[i].childControls.length;k++){
          arrayValidatora = [];
          for(var j=0;j<this.formConfig[i].childControls[k].validators.length;j++){
            arrayValidatora.push(Validators[this.formConfig[i].childControls[k].validators[j]]);
          }
          subgroupObject[this.formConfig[i].childControls[k].key] = new FormControl('',arrayValidatora);
      }
      testFormObject[this.formConfig[i].name] = new FormGroup(subgroupObject);
    }
   
  } */

  
  ngOnInit(){
    console.log(formConfig);
    this.form = formConfig;
    let testFormObject = {};
    var arrayValidatora;
    for (var i = 0; i < this.form.length; i++) {
      arrayValidatora = [];
      for (var j = 0; j < this.form[i].validators.length; j++) {
        arrayValidatora.push(Validators[this.form[i].validators[j]]);
      }
      testFormObject[this.form[i].key] = new FormControl("",arrayValidatora
      );
    }
    this.newForm = new FormGroup(testFormObject);
    console.log("this newForm",this.newForm.value);
  }

  saveNew(data) {
    

     console.log("data newForm",data.value.profilePicture);
    this.contact = data.value;
    
    console.log("asdfasdfas",this.contact);
    console.log(this.submitTried)
   /*  this.contactsModel.saveContact(this.contact,() =>{
      console.log(this.contact);
      
     // this.router.navigate(["/contacts"])
    }) */
    //this.submitTried = true;
    
   }
}
