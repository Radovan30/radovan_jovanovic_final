import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ContactsModel } from '../../models/contacts/contacts.model';

@Component({
  selector: 'app-contactinfo',
  templateUrl: './info.html'
})
export class ContactinfoComponent implements OnInit {
  contact = [];
  id;
  constructor(private route:Router, private aroute:ActivatedRoute, private contactsModel:ContactsModel){  }
  ngOnInit(){
    this.aroute.parent.params.subscribe(params => {
      this.id=params.id;
      this.contactsModel.getContactById(params.id, a => {
        console.log("params id",params.id);
        a.birthdate = new Date(a.birthdate).toLocaleDateString();
        
        
        this.contact = a;
        console.log("asdfasdfasdfas",this.contact);
        
      });
    });
}
deleteContact(id){
  var r = confirm("Delete contact with ID: "+ id + "?");
  if (r == true) {
    this.contactsModel.deleteContact(id, () => {
      this.route.navigate["/contacts"];
    });
  } else {
    this.route.navigate["/contacts"];
  }
}
editContact(id) {
  console.log(id);
  
  this.route.navigate(["/contact", id, "edit"]);
}
}
