import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
@Injectable()
export class ContactsService {

  constructor(private http:Http){}

    getContactsObservable(){
        return this.http.get('http://localhost:3000/contacts')
        .map((response)=>{
            console.log(response);
            
            return response.json();
        });
    }

    getContactObservable(id){
        console.log(id);
        return this.http.get('http://localhost:3000/contacts/'+id) .map((response)=>{
            var objekat = response.json();
            console.log("objekat", objekat);
            var result = [];
            for(let key in objekat){
                result.push({label:objekat[key], key:objekat[key]});
            }
            
            console.log("get contact",result);
            
            return result;
        });
    }
    getEditContact(id){
        return this.http.get('http://localhost:3000/contacts/'+id).map((response) => { return response.json()});
    }
    addContactObeservable(data){
        return this.http.post('http://localhost:3000/contacts/',data).map((response) => { return response.json()});
    }
    updateContactObservable(data){
        console.log(data)
        return this.http.put('http://localhost:3000/contacts/'+ data.id, data).map((response) => {  response.json()});
        
    }
    deleteContactObservable(id){
        return this.http.delete('http://localhost:3000/contacts/'+id).map((response) => { return response.json()});
    }
}
