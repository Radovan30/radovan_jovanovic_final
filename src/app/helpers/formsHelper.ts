export default [
{

    label:'First name',
    key: 'firstname',
    validators:['required'],
    type:'text'
},{
  
    label:'Last name',
    key: 'lastname',
    validators:['required'],
    type:'text'
},{
  
    label:'Age',
    key: 'age',
    validators:['required'],
    type:'text'
},{
   
    label:'Picture(url)',
    key: 'profilePicture',
    validators:['required'],
    type:'text'
},{
   
    label:'Birthdate',
    key: 'birthdate',
    validators:['required'],
    type:'date',
    options:{
        dateFormat: 'dd/mm/yyyy',
        inline:false,
        openSelectorTopOfInput:true,
        openSelectorOnInputClick:true
    }
}
]